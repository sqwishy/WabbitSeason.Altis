/*
 * :)
 */

#define WABBIT_TERRAIN_SPAWNS ["TREE", "SMALL TREE", "BUSH"]
#define WABBIT_SMOKE_EFFECTS ["SmokeShellPurple", "SmokeShellGreen", "SmokeShellBlue", "SmokeShellOrange", "SmokeShellRed", "SmokeShellYellow"]

#define VERBOSE

#ifdef VERBOSE
log_wabbit_info = {
    diag_log (format ["INFO:WABBIT - %1", format _this]);
};
#endif

#ifndef VERBOSE
log_wabbit_info = {};
#endif

// WabbitLib

#define CHAOS_MODULUS    2^31
#define CHAOS_INCREMENT  12345
#define CHAOS_MULTIPLIER 1103515245

WabbitLib_fnc_chaos = {
    (CHAOS_MULTIPLIER * _this + CHAOS_INCREMENT) mod CHAOS_MODULUS
};

WabbitLib_fnc_invokeAtInterval = {
    // assert canSuspend; TODO 1.57
    params ["_interval", "_args", "_callable"];
    private _last_tick = time;
    while {true} do {
        private _now = time;
        private _thread = _args spawn _callable;
        _last_tick = _now;
        waitUntil {sleep 0.01; scriptDone _thread};
        sleep (_last_tick + _interval - time);
    };
};

WabbitLib_fnc_conditionalSelectIP = {
    params ["_collection", "_test"];
    if !(_collection isEqualTo []) then {
        private _forEachIndex = 0;
        while {_forEachIndex < count _collection} do {
            private _x = _collection select _forEachIndex;
            if (call _test) then {
                _forEachIndex = _forEachIndex + 1;
            } else {
                _collection deleteAt _forEachIndex;
            };
        };
    };
    _collection
};

WabbitLib_fnc_weightedRandom = {
    params ["_weights"];

    if (count _weights < 1) exitWith { -1 };

    private _sum = 0;
    {
        _sum = _sum + _x;
    } forEach _weights;

    private _threshold = random _sum;
    private _cursor = 0;

    private _idx =
        {
            _cursor = _cursor + _x;
            if (_cursor >= _threshold) exitWith {
                _forEachIndex
            };
            // I think the only case we would get here at the end of the loop is if
            // there is an issue with floating point precision. In that case, just
            // return the index of the last element as that probably was was
            // selected.
            (count _weights - 1)
        } forEach _weights;

    _idx
};

WabbitLib_fnc_startsWith = {
    params ["_txt", "_start"];
    if (count _txt < count _start) then {
        false
    } else {
        private _a = toArray _txt;
        _a resize (count _start);
        _a isEqualTo (toArray _start)
    }
};

WabbitLib_fnc_newNamespace = {
    // Poor man's namespace, borrowed from https://github.com/CBATeam/CBA_A3/pull/260
    // TODO, will this have weird side-effects?
    createLocation ["Name", [0, 0, 0], 0, 0]
};

WabbitLib_fnc_canSeePositionASL = {
    params ["_unit", "_targetASL"];
    not (lineIntersects [eyePos _unit, _targetASL])
};

WabbitLib_fnc_isVisibleToPlayers = {
    params ["_targetASL"];
    private _isSeen =
        {
            if ([_x, _targetASL] call WabbitLib_fnc_canSeePositionASL) exitWith {true};
        } forEach allPlayers;
    if (isNil "_isSeen") exitWith {false};
    _isSeen
};

WabbitLib_fnc_fixupSpawnPosition = {
    params ["_pos_atl"];
    private _attempts = 0;
    while {_attempts = _attempts + 1; _attempts <= 20} do {
        private _dist = 0.7 + (_attempts / 4);
        private _theta = random 360;
        private _check_pos = _pos_atl vectorAdd [_dist * sin _theta, _dist * cos _theta, 0];
        if !(lineIntersects [(ATLtoASL _check_pos) vectorAdd [0, 0, 0.1],
                             (ATLtoASL _check_pos) vectorAdd [0, 0, 1]]) exitWith {
            _pos_atl = _check_pos;
        };
    };
    _pos_atl
};

WabbitLib_fnc_possibleWabbitSpawnPositions = {
    params ["_origin", "_radius", ["_limit", 0]];
    private _found = [];

    _found append (
        (nearestTerrainObjects [_origin, WABBIT_TERRAIN_SPAWNS, _radius])
        apply
        {[getPosATL _x] call WabbitLib_fnc_fixupSpawnPosition}
    );

    // TODO, we might need something like this for urban areas

    //private _buildings = _origin nearObjects ["Building", _radius];
    //_buildings = [_buildings, {_x isEqualTo (nearestBuilding _x)}] call BIS_fnc_conditionalSelect;
    //{
    //    private _positions = _x call BIS_fnc_buildingPositions;
    //    _found append _positions;
    //} forEach _buildings;

    _found
};

WabbitLib_fnc_nearbyWabbits = {
    params ["_subject", "_radius"];
    private _value = 0;
    {
        _value = _value + ((1 - ((_subject distance _x) / _radius)) ^ 2);
    } forEach (_subject nearObjects ["Rabbit_F", _radius]);
    _value
};

WabbitLib_fnc_showDeadWabbit = {
    params ["_wabbit"];
    // For some reason there is delay after the wabbit dies before this
    // animation plays and it's kind of annoying. So we force the animation.
    _wabbit switchMove "Rabbit_Die";
};

WabbitLib_fnc_explodeTarget = {
    params ["_target", ["_height_offset", 14]];
    private _bb = boundingBox _target;
    private _height = (_bb select 1 select 2) - (_bb select 0 select 2);

    private _aim_pos = (getPosATL _target) vectorAdd ((velocity _target) vectorMultiply (0.5 + random 1));
    private _proj_pos = (getPosATL _target) vectorAdd [0, 0, _height + _height_offset];
    private _proj = "M_Titan_AT" createVehicle _proj_pos;
    private _vector_dir = _proj_pos vectorFromTo _aim_pos;
    private _vector_up = if (_vector_dir isEqualTo [0, 0, -1]) then {[1, 0, 0]} else {[0, 0, 1]};
    diag_log [_vector_dir, _vector_up];
    _proj setVectorDirAndUp [_vector_dir, _vector_up];
    _proj
};

// WabbitHunter

WabbitHunter_fnc_init = {
    params ["_handle", "_player", "_area"];
    private _self = call WabbitLib_fnc_newNamespace;
    _self setVariable ["handle", _handle];
    _self setVariable ["player", _player];
    _self
};

// WabbitDetectionGrid :)

// Width/height of a detection grid. The distance between each detection grid.
DETECTION_GRID_STEP = 25;

WabbitDetectionGrid_fnc_init = {
    params ["_handle", "_area"];
    private _self = call WabbitLib_fnc_newNamespace;
    _self setVariable ["handle", _handle];
    _self setVariable ["game_area", _area];
    _self setVariable ["markers", []];
    _self
};

WabbitDetectionGrid_fnc_updateMarkers = {
    params ["_self"];

    (_self getVariable "game_area") params ["_origin", "_radius"];
    private _handle = _self getVariable "handle";
    private _markers = _self getVariable "markers";

    private _wabbits = _origin nearObjects ["Rabbit_F", _radius + 75];
    [_wabbits, {(alive _x) and {(_x getVariable ["wabbity", -1]) >= 0}}] call WabbitLib_fnc_conditionalSelectIP;

    private _new = [];
    private _updated = [];
    {
        private _serial = _x getVariable ["wabbity", -1];
        // It's imperitive that we use mod here because Arma is not capable
        // of making sense of even slightly large numbers.
        private _chaos = (_serial call WabbitLib_fnc_chaos) mod 360;
        private _time = if (isMultiplayer) then {serverTime} else {time};
        private _amplitude = (5 + _chaos mod 15) * cos (20 * _time + _chaos);
        private _freq = 4 * sin (10 * _time + _chaos);
        private _pos = (getPosATL _x) vectorAdd [_amplitude * sin (_freq * _time + _chaos), _amplitude * cos (_freq * _time + _chaos), 0];
        private _mkr = format ["%1.mkr%2", _handle, _serial];
        if (_mkr in _markers) then {
            //["Updated marker: %1", _mkr] call log_wabbit_info;
            _updated pushBack _mkr;
        } else {
            //["New marker: %1", _mkr] call log_wabbit_info;
            createMarkerLocal [_mkr, _pos];
            _mkr setMarkerColorLocal "ColorOPFOR";
            _mkr setMarkerShapeLocal "ICON";
            _mkr setMarkerTypeLocal "mil_triangle";
            _new pushBack _mkr;
        };
        _mkr setMarkerPosLocal _pos;
    } forEach _wabbits;

    [
        _markers,
        {
            if (_x in _updated) then
            {
                true
            } else {
                deleteMarkerLocal _x;
                ["Deleted marker: %1", _x] call log_wabbit_info;
                false
            };
        }
    ] call WabbitLib_fnc_conditionalSelectIP;

    _markers append _new;
};

WabbitDetectionGrid_fnc_tickForeverEvery = {
    params ["_self", "_interval"];
    [_interval, [_self], WabbitDetectionGrid_fnc_updateMarkers]
        call WabbitLib_fnc_invokeAtInterval;
};

// WabbitServer

WabbitServer_fnc_init = {
    params ["_handle", "_area"];
    _area params ["_origin", "_radius"];
    private _self = call WabbitLib_fnc_newNamespace;
    _self setVariable ["handle", _handle];
    _self setVariable ["game_area", _area];
    _self setVariable ["player_bounds", _radius * 2 + 50];
    _self setVariable ["wabbits", []];
    _self setVariable ["wabbit_limit", 30];
    _self setVariable ["wabbit_side", resistance];
    _self setVariable ["wabbit_serial", 0];
    _self setVariable ["wabbit_group", createGroup resistance];
    _self setVariable ["possible_spawns", [_origin, _radius] call WabbitLib_fnc_possibleWabbitSpawnPositions];
    if ([] isEqualTo (_self getVariable "possible_spawns")) then {
        ["No possible wabbit spawns found %1 metres around %2.", _radius, _origin] call BIS_fnc_error;
    };
    _self
};

WabbitServer_fnc_tickOnce = {
    params ["_srv"];
    [_srv] call WabbitServer_fnc_removeOutOfBoundsWabbits;
    [_srv] call WabbitServer_fnc_removeOutOfBoundsPlayers;
    [_srv] call WabbitServer_fnc_repopulateWabbits;
};

WabbitServer_fnc_tickForeverEvery = {
    params ["_srv", "_interval"];
    [_interval, [_srv], WabbitServer_fnc_tickOnce]
        call WabbitLib_fnc_invokeAtInterval;
};

WabbitServer_fnc_removeOutOfBoundsWabbits = {
    params ["_srv"];
    (_srv getVariable "game_area") params ["_origin", "_radius"];
    {
        if ((_origin distance _x) > _radius) then {
            ["Wabbit %1 is out of bounds, trying to kill.", _x] call log_wabbit_info;
            [_x] spawn WabbitLib_fnc_explodeTarget;
        };
    } forEach (_srv getVariable "wabbits");
};

WabbitServer_fnc_removeOutOfBoundsPlayers = {
    params ["_srv"];
    (_srv getVariable "game_area") params ["_origin", "_radius"];
    private _bounds = _srv getVariable "player_bounds";
    {
        if ((alive _x) and {(_origin distance _x) > _bounds}) then {
            ["Player %1 is out of bounds, trying to kill.", _x] call log_wabbit_info;
            [_x] spawn WabbitLib_fnc_explodeTarget;
        };
    } forEach allPlayers;
};

WabbitServer_fnc_repopulateWabbits = {
    // TODO, this should probably lock something as it is not threadsafe
    params ["_srv"];
    private _wabbits = _srv getVariable "wabbits";
    [_wabbits, {alive _x}] call WabbitLib_fnc_conditionalSelectIP;
    private _deficit =  ([_srv] call WabbitServer_fnc_desiredWabbitCount) - (count _wabbits);
    if (_deficit > 0) then {
        private _new_wabbits = [_srv, _deficit] call WabbitServer_fnc_spawnWabbitsAppropriately;
        _wabbits append _new_wabbits;
        ["Noticed a deficit of %1 wabbits - created %2 more", _deficit, count _new_wabbits] call log_wabbit_info;
    };
};

WabbitServer_fnc_spawnWabbit = {
    params ["_srv", "_pos"];
    ["WabbitServer_fnc_spawnWabbit at %1", _pos] call log_wabbit_info;
    private _wabbit = (_srv getVariable "wabbit_group") createUnit ["Rabbit_F", _pos, [], 0, "NONE"];
    private _wabbit_serial = _srv getVariable "wabbit_serial";
    _srv setVariable ["wabbit_serial", _wabbit_serial + 1];
    _wabbit setVariable ["wabbity", _wabbit_serial, true];
    _wabbit setPosATL _pos;
    // Give the little fucker a bit of crack
    // This doesn't work :(
    _wabbit setAnimSpeedCoef 4.0;
    // Yuck
    private _handler = format ["[%1, _this] call WabbitServer_fnc_handleWabbitKilled", _srv getVariable "handle"];
    _wabbit addEventHandler ["Killed", _handler];
    _wabbit
};

WabbitServer_fnc_handleWabbitKilled = {
    params ["_srv", "_event"];
    _event params ["_wabbit", "_killer"];
    ["Wabbit %1 was slain by %2", _wabbit, _killer] call log_wabbit_info;

    // Make the wabbit look dead
    [_wabbit] remoteExecCall ["WabbitLib_fnc_showDeadWabbit"];

    // Shoot a smoke grenade out of its butt
    private _effect = (selectRandom WABBIT_SMOKE_EFFECTS) createVehicle (getPosATL _wabbit);
    _effect setVelocity [0, 0, 10 + random 15];

    if (isPlayer _killer) then {
        _killer addScore 1;
    };
};

WabbitServer_fnc_spawnWabbitsAppropriately = {
    params ["_srv", "_num_wabbits"];

    private _spawns =
        [_srv getVariable "possible_spawns", {not ([ATLtoASL (_x vectorAdd [0, 0, 1])] call WabbitLib_fnc_isVisibleToPlayers)}]
            call BIS_fnc_conditionalSelect;

    if (_spawns isEqualTo []) exitWith {[]};

    private _spawned = [];

    while {count _spawned < _num_wabbits} do {
        private _pos = selectRandom _spawns;
        private _wabbit = [_srv, _pos] call WabbitServer_fnc_spawnWabbit;
        _spawned pushBack _wabbit;
    };

    _spawned
};

WabbitServer_fnc_desiredWabbitCount = {
    params ["_srv"];
    _srv getVariable "wabbit_limit"
};
