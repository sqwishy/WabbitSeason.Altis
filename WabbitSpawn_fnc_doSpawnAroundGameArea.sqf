#define MAX_ATTEMPTS 50

params ["_unit", "_old_unit", "_idfk", "_respawnDelay", ["_revive", false]];
diag_log "WabbitSpawn_fnc_doSpawnAroundGameArea";
diag_log _this;
diag_log (simulationEnabled _unit);

if !(isPlayer _unit) exitWith {};

if (
    ([] call BIS_fnc_reviveEnabled)
    and {
        ((player getVariable ["#rev", -1]) > 0)
        or (isNull _old_unit)
        }
    ) exitWith {};

// This is cheating a little bit
wabbit_game_area params ["_origin", "_radius"];
private _attempts = 0;
private _transport = objNull;
while {_attempts = _attempts + 1; _attempts <= MAX_ATTEMPTS} do {
    private _dist = _radius * (random 0.5 + 1.5);
    private _theta = random 360;
    private _pos = [
        (_origin select 0) + _dist * (sin _theta),
        (_origin select 1) + _dist * (cos _theta),
        1.5
    ];
    if (getTerrainHeightASL _pos > 0) exitWith {
        _transport = createVehicle ["C_Quadbike_01_F", _pos, [], 0, "NONE"];
        _unit moveInDriver _transport;
        _transport setDir (_pos getDir _origin);
        _transport engineOn true;
    };
};
if (isNull _transport) then {
    ["Failed %1 times to find a valid spawn location! Giving up.", MAX_ATTEMPTS] call BIS_fnc_error;
};
