params ["_unit", "_old_unit", "_idfk", "_respawnDelay", ["_revive", false]];
diag_log "WabbitSpawn_fnc_doGiveRandomLoadout";
diag_log _this;

if !(isplayer _unit) exitWith {};

if (
    ([] call BIS_fnc_reviveEnabled)
    and {
        ((player getVariable ["#rev", -1]) > 0)
        or (isNull _old_unit)
        }
    ) exitWith {};

if (isNil "WabbitSpawn_doGiveRandomLoadout__cached") then {
    WabbitSpawn_doGiveRandomLoadout__goggles =
        "true" configClasses (configFile >> "CfgGlasses");
    WabbitSpawn_doGiveRandomLoadout__vests =
        ("(getNumber (_x >> ""scope"") >= 2) and (701 == getNumber (_x >> ""itemInfo"" >> ""type""))" configClasses (configFile >> "CfgWeapons"));
    WabbitSpawn_doGiveRandomLoadout__uniforms =
        ("(getNumber (_x >> ""scope"") >= 2) and (801 == getNumber (_x >> ""itemInfo"" >> ""type""))" configClasses (configFile >> "CfgWeapons"));
    WabbitSpawn_doGiveRandomLoadout__headgear =
        ("(getNumber (_x >> ""scope"") >= 2) and (605 == getNumber (_x >> ""itemInfo"" >> ""type""))" configClasses (configFile >> "CfgWeapons"));
    WabbitSpawn_doGiveRandomLoadout__rifles = [
        configFile >> "CfgWeapons" >> "SMG_01_F",
        configFile >> "CfgWeapons" >> "SMG_02_F",
        configFile >> "CfgWeapons" >> "srifle_DMR_01_F",
        configFile >> "CfgWeapons" >> "srifle_DMR_02_F",
        configFile >> "CfgWeapons" >> "srifle_DMR_03_F",
        configFile >> "CfgWeapons" >> "srifle_EBR_F",
        configFile >> "CfgWeapons" >> "LMG_Mk200_F",
        configFile >> "CfgWeapons" >> "arifle_TRG21_F",
        configFile >> "CfgWeapons" >> "arifle_TRG20_F",
        configFile >> "CfgWeapons" >> "arifle_MXM_F",
        configFile >> "CfgWeapons" >> "arifle_MXM_Black_F",
        configFile >> "CfgWeapons" >> "arifle_MXC_F",
        configFile >> "CfgWeapons" >> "arifle_MXC_Black_F",
        configFile >> "CfgWeapons" >> "arifle_Katiba_F",
        configFile >> "CfgWeapons" >> "arifle_Katiba_C_F"
    ];

    WabbitSpawn_doGiveRandomLoadout__cached = true;
};

removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

// Containers and other doodads
if (random 1 < 0.98) then {
    _unit forceAddUniform (configName selectRandom WabbitSpawn_doGiveRandomLoadout__uniforms);
};
_unit addItemToUniform "FirstAidKit";
_unit addItemToUniform "SmokeShellBlue";
_unit addItemToUniform "SmokeShellRed";
_unit addVest (configName selectRandom WabbitSpawn_doGiveRandomLoadout__vests);
_unit addHeadgear (configName selectRandom WabbitSpawn_doGiveRandomLoadout__headgear);
_unit addGoggles (configName selectRandom WabbitSpawn_doGiveRandomLoadout__goggles),

// Ammo
_unit addItem "16Rnd_9x21_Mag";
_unit addItem "16Rnd_9x21_Mag";
_unit addItem "16Rnd_9x21_Mag";

private _primary_weapon_config = selectRandom WabbitSpawn_doGiveRandomLoadout__rifles;
private _primary_ammo_mass = 80 + floor random 40;
while {_primary_ammo_mass > 0} do {
    private _muzzle = selectRandom (getArray (_primary_weapon_config >> "muzzles"));
    private _magazine =
        if (_muzzle isEqualTo "this") then {
            selectRandom (getArray (_primary_weapon_config >> "magazines"))
        } else {
            selectRandom (getArray (_primary_weapon_config >> _muzzle >> "magazines"))
        };
    _primary_ammo_mass = _primary_ammo_mass - getNumber (configFile >> "CfgMagazines" >> _magazine >> "mass");
    diag_log [_primary_weapon_config, _primary_ammo_mass, _magazine, _unit canAdd _magazine];
    if !(_unit canAdd _magazine) exitWith {};
    _unit addItem _magazine;
};

// Primary weapon
_unit addWeapon (configName _primary_weapon_config);

// Attach suppressor, flashlight/lazer, mounty resty sticky thingy.
{
    if (random 1 < .50) then {
        private _items = getArray (_primary_weapon_config >> "WeaponSlotsInfo" >> _x >> "compatibleItems");
        if !(_items isEqualTo []) then {
            _unit addPrimaryWeaponItem (selectRandom _items);
        };
    };
} forEach ["MuzzleSlot", "PointerSlot", "UnderBarrelSlot"];

// Attach optics
if (random 1 < .70) then {
    private _items = getArray (_primary_weapon_config >> "WeaponSlotsInfo" >> "CowsSlot" >> "compatibleItems");
    // Whitelist the available optics
    _items = _items arrayIntersect ["optic_Holosight", "optic_ACO", "optic_ACO_grn"];
    if !(_items isEqualTo []) then {
        _unit addPrimaryWeaponItem (selectRandom _items);
    };
};

// Secondary weapon
_unit addWeapon "hgun_P07_F";
if (random 1 < 0.20) then {
    _unit addHandgunItem "muzzle_snds_L";
};

// Other shit
_unit addWeapon "Binocular";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "ItemRadio";
_unit linkItem "ItemGPS";
_unit linkItem "NVGoggles_OPFOR";
