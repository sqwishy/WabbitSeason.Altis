WS_fnc_loadFile = {
    call compile preprocessFileLineNumbers _this;
    diag_log (format ["WS_fnc_loadFile loaded %1", _this]);
};

"lib_wabbits.sqf" call WS_fnc_loadFile;

//[missionNamespace, "Rifleman"] call BIS_fnc_addRespawnInventory;

// The first arg here should be a 2d position
wabbit_game_area = [getMarkerPos "GAME_AREA_ORIGIN", 150];

if (isServer) then {
    0 spawn {
        wabbit_srv = ["wabbit_srv", wabbit_game_area] call WabbitServer_fnc_init;
        [wabbit_srv, 4] call WabbitServer_fnc_tickForeverEvery;
    };
};

if (isServer && isMultiplayer) then
{
    ["InitializePlayer"] call BIS_fnc_dynamicGroups;
};

if (hasInterface) then {
    0 spawn {
        waitUntil {not isNull player};

        wabbit_hunter = ["wabbit_hunter", player] call WabbitHunter_fnc_init;

        wabbit_detection_grid = ["wabbit_detection_grid", wabbit_game_area] call WabbitDetectionGrid_fnc_init;
        [wabbit_detection_grid, 1] spawn WabbitDetectionGrid_fnc_tickForeverEvery;

        [player, wabbit_game_area select 0] call BIS_fnc_addRespawnPosition;

        // Dynamic Groups (thank you BIS)
        ["InitializePlayer", [player]] call BIS_fnc_dynamicGroups;

        // Briefing
        player createDiaryRecord
            ["Diary", ["Mission", [
                "It's wabbit season, and you're hunting wabbits.",
                "Wabbits are tracked (inaccurately) on the map and marked with triangles.",
                "Killing wabbits gets you one point per slain wabbit.",
                "Any life that wanders too far from the game area is executed."
                ] joinString "<br/>"]];
    };
};

//

is_ace_enabled = isClass (configfile >> "CfgPatches" >> "ace_common");

// ACE settings

if (is_ace_enabled) then
{
    ace_map_BFT_Enabled = true;
    ace_map_BFT_Interval = 4;
    ace_map_BFT_HideAIGroups = false;

    ace_map_MapShowCursorCoordinates = true;

    ace_advanced_balistics_Enabled = true;

    ace_medical_EnableRevive = 1;

    BIS_revive_enabled = false;
};
